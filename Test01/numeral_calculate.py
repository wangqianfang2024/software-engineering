import sys


def get_arr():
    arg_list = eval(str(sys.argv))
    if len(arg_list) > 2:
        print("参数输入有误1")
        sys.exit()
    return arg_list[-1]


def calculate():
    try:
        arr = get_arr()
        arr = eval(arr)
        n = len(arr)
        res = -float('inf')
        for i in range(n):
            j = i + 1
            cnt = arr[i]
            while j < n:
                cnt += arr[j]
                res = max(res, cnt)
                j += 1
        print(f'The original arr is : {arr}')
        print(f'The sum of the subarrays in this array is at most : {res}')
    except Exception as e:
        print("输入的参数有误！", e)


if __name__ == '__main__':
    calculate()
