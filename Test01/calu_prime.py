import time


def calculate_way1(up):
    cnt = 0
    start_t = time.time()
    for i in range(2, up + 1):
        flag = True
        for j in range(2, i):
            if i % j == 0:
                flag = False
                break
        if flag:
            cnt += 1
            print(f'{i} ', end='') if cnt % 5 else print(i)
    print("\nThe total time consumed is ", time.time() - start_t)


def calculate_way2(up):
    start_t = time.time()
    prime_list = []  # 素数表
    for i in range(2, up):
        for x in prime_list:
            if i % x == 0:
                break
        else:
            prime_list.append(i)
            print(f'{i} ', end='') if len(prime_list) % 5 else print(i)
    print("\nThe total time consumed is ", time.time() - start_t)


if __name__ == '__main__':
    calculate_way2(20000)
